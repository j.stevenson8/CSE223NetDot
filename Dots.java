//John Stevenson
//CSE 223
//PA5 
//Net Dots

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import javax.swing.ButtonGroup;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Dots extends JFrame {

	private JPanel contentPane;
	private JTextField serverTextField;
	private JTextField clientTextField;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField hostTextField;
	private Server server;
	private Client client;
	private boolean isServer = false;
	private boolean gameStart = false;
	private boolean isTurn = false;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Dots frame = new Dots();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Dots() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 734, 587);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		int width = 8;
		int length = 8;
		Box[][] board = new Box[width][length];
		for(int i = 0; i < width; i++) {
			for(int j = 0; j < length ; j++) {
				board[i][j] = new Box();
			}
		}
		
		JButton startButton = new JButton("Start");
		
		serverTextField = new JTextField();
		serverTextField.setText("Server Name");
		serverTextField.setBounds(350, 10, 100, 25);
		contentPane.add(serverTextField);
		serverTextField.setColumns(10);
		
		clientTextField = new JTextField();
		clientTextField.setEditable(false);
		clientTextField.setText("Client Name");
		clientTextField.setBounds(475, 10, 100, 25);
		contentPane.add(clientTextField);
		clientTextField.setColumns(10);
	
		JLabel serverScoreTextField = new JLabel("Server Score : 0");
		serverScoreTextField.setEnabled(false);
		serverScoreTextField.setBounds(10, 10, 250, 25);
		contentPane.add(serverScoreTextField);
	
		JLabel clientScoreTextField = new JLabel("Client Score : 0");
		clientScoreTextField.setEnabled(false);
		clientScoreTextField.setBounds(10, 40, 250, 25);
		contentPane.add(clientScoreTextField);
		
		JRadioButton serverRadioButton = new JRadioButton("Server");
		serverRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) { //When the server button is pressed, suppress all client operations and change button to "Start"
				clientTextField.setEditable(false);
				serverTextField.setEditable(true);
				hostTextField.setVisible(false);
				startButton.setText("Start");
			}
		});
		buttonGroup.add(serverRadioButton);
		serverRadioButton.setSelected(true);
		serverRadioButton.setBounds(350, 45, 100, 23);
		contentPane.add(serverRadioButton);
		
		JRadioButton clientRadioButton = new JRadioButton("Client");
		clientRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { //When the client button is pressed, bring forth all client operations and change button to "Connect"
				clientTextField.setEditable(true);
				serverTextField.setEditable(false);
				hostTextField.setVisible(true);
				startButton.setText("Connect");				
			}
		});
		buttonGroup.add(clientRadioButton);
		clientRadioButton.setBounds(475, 45, 100, 25);
		contentPane.add(clientRadioButton);
		
		GamePanel gamePanel = new GamePanel(board, length, width);
		gamePanel.setBounds(30, 76, 495, 446);
		contentPane.add(gamePanel);
		//gamePanel.setVisible(false);
		
		startButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) { //When the start button is pressed, create either a socket or a server socket
				if(startButton.getText().equals("Start")) { //If this is the server, create a server socket
					isServer = true;
					System.out.println("Server");
					try {
						//System.out.println("Attempting to create server.");
						server = new Server(serverTextField.getText(),board, gamePanel);
					}
					catch(Exception e) {
						//System.out.println("Failed to create server");
						System.exit(0);
						return;
					}
					////System.out.println("Server creation code complete.");
					serverScoreTextField.setText(server.serverName + "'s Score: 0");
					serverScoreTextField.setVisible(true);
					clientScoreTextField.setText(server.clientName + "'s Score: 0");
					clientScoreTextField.setVisible(true);		
					gamePanel.setVisible(true);
					startButton.setVisible(false);
					gameStart = true;
					//System.out.println("Game has begun.");
					isTurn = true;
					return;
				}
				else {
					try {
						System.out.println("Client.");
						client = new Client(clientTextField.getText(), hostTextField.getText(), board, gamePanel);
					}
					catch (Exception e) {
						//System.out.println("Failed to create client");
						System.exit(0);
						return;
					}
					serverScoreTextField.setText(client.serverName + "'s Score: 0");
					serverScoreTextField.setVisible(true);
					clientScoreTextField.setText(client.clientName + "'s Score: 0");
					clientScoreTextField.setVisible(true);	
					gamePanel.setVisible(true);
					startButton.setVisible(false);
					hostTextField.setVisible(false);
					//System.out.println("Game has begun.");
					client.waitForInput();
					gameStart = true;
					isTurn = true;
					return;
				}
			}
		});
		startButton.setBounds(600, 10, 100, 25);
		contentPane.add(startButton);
		
		hostTextField = new JTextField();
		hostTextField.setText("localhost");
		hostTextField.setBounds(600, 45, 100, 25);
		contentPane.add(hostTextField);
		hostTextField.setColumns(10);
		hostTextField.setVisible(false);
		
		gamePanel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) { //when a player clicks on the game board, handle it
				if((!gameStart) || (!isTurn)) {
					return;
				}
				int serverScore = 0;
				int clientScore = 0;
				
				if(isServer) {
					for(int i = 0; i < width; i++) {
						for(int j = 0; j < length; j++) {
							if(board[i][j].getOwner() == server.serverName.charAt(0)) {
								serverScore++;
							}
							if(board[i][j].getOwner() == server.clientName.charAt(0)) {
								clientScore++;
							}
							
						}
					}
					serverScoreTextField.setText(server.serverName + "'s Score: " + serverScore);
					clientScoreTextField.setText(server.clientName + "'s Score: " + clientScore);
				}
				else {
					for(int i = 0; i < width; i++) {
						for(int j = 0; j < length; j++) {
							if(board[i][j].getOwner() == client.serverName.charAt(0)) {
								serverScore++;
							}
							if(board[i][j].getOwner() == client.clientName.charAt(0)) {
								clientScore++;
							}
						}
					}
					serverScoreTextField.setText(client.serverName + "'s Score: " + serverScore);
					clientScoreTextField.setText(client.clientName + "'s Score: " + clientScore);
				}
				
	
				//System.out.println("Mouse click registered.");
				
				int X = arg0.getX();
				int Y = arg0.getY();
			
				////System.out.println("Mouse clicked at (" + X + "," + Y + ")");
				
				int boxX = (X - 15) / 50;
				int boxY = (Y - 15) / 50;
				//'//System.out.println("Box[" + boxX + "][" + boxY + "]");
				
				int distX = (X - 15) % 50;
				int distY = (Y - 15) % 50;
				
				int invDistX = 50 - distX;
				int invDistY = 50 - distY;
				////System.out.println("DistX = " + distX);
				////System.out.println("DistY = " + distY);
				////System.out.println("invDistX = " + invDistX);
				////System.out.println("invDistY = " + invDistY);
				
				boolean completed = false;
				
				if((distX < distY) && (distX < invDistX) && (distX < invDistY)) { //click was closest to the left side
					if(!board[boxX][boxY].isLeft()) { //if this side has not already been clicked, click it
						////System.out.println("Clicked board[" + boxX + "][" + boxY + "] left side.");
						board[boxX][boxY].clickLeft();
						if(isServer) {
							//System.out.println("Server clicked left.");
							server.output.println(boxX);
							server.output.flush();
							server.output.println(boxY);
							server.output.flush();
							server.output.println("Left");
							server.output.flush();
						}
						else {
							//System.out.println("Client clicked left.");
							client.output.println(boxX);
							client.output.flush();
							client.output.println(boxY);
							client.output.flush();
							client.output.println("Left");
							client.output.flush();
						}
						
						if(boxX != 0) {
							board[boxX-1][boxY].clickRight();
						}
						////System.out.println("Left side clicked.");
						if(board[boxX][boxY].isClosed()) {
							if(isServer) {
								//System.out.println("Server completed a box.");
								board[boxX][boxY].setOwner(server.serverName.charAt(0)); //set the owner 
								completed = true;
							}
							else {
								//System.out.println("Client completed a box.");
								board[boxX][boxY].setOwner(client.clientName.charAt(0)); //set the owner 
								completed = true;
							}
						}
						if(boxX != 0) {
							if(board[boxX-1][boxY].isClosed()) {
								if(isServer) {
									//System.out.println("Server completed a box.");
									board[boxX-1][boxY].setOwner(server.serverName.charAt(0)); //set the owner 
								completed = true;
								}
								else {
									//System.out.println("Client completed a box.");
									board[boxX-1][boxY].setOwner(client.clientName.charAt(0)); //set the owner 
								completed = true;
								}
							}
						}
					}
				}
				else if((distY < distX) && (distY < invDistX) && (distY < invDistY)) { //click was closest to the top side
					if(!board[boxX][boxY].isTop()) { //if this side has not already been clicked, click it
						////System.out.println("Clicked board[" + boxX + "][" + boxY + "] top side.");
						board[boxX][boxY].clickTop();
						//System.out.println("Clicked top.");
						if(isServer) {
							//System.out.println("Server clicked top.");
							server.output.println(boxX);
							server.output.flush();
							server.output.println(boxY);
							server.output.flush();
							server.output.println("Top");
							server.output.flush();
						}
						else {
							//System.out.println("Client clicked top.");
							client.output.println(boxX);
							client.output.flush();
							client.output.println(boxY);
							client.output.flush();
							client.output.println("Top");
							client.output.flush();
						}

						if(boxY != 0) {
							board[boxX][boxY-1].clickTop();
						}
						
						if(board[boxX][boxY].isClosed()) {
							if(isServer) {
								//System.out.println("Server completed a box.");
								board[boxX][boxY].setOwner(server.serverName.charAt(0)); //set the owner 
								completed = true;
							}
							else {
								//System.out.println("Client completed a box.");
								board[boxX][boxY].setOwner(client.clientName.charAt(0)); //set the owner 
								completed = true;
							}
						}
						if(boxY != 0) {
							if(board[boxX][boxY-1].isClosed()) {
								if(isServer) {
									//System.out.println("Server completed a box.");
									board[boxX][boxY-1].setOwner(server.serverName.charAt(0)); //set the owner 
									completed = true;
								}
								else {
									//System.out.println("Client completed a box.");
									board[boxX][boxY-1].setOwner(client.clientName.charAt(0)); //set the owner 
									completed = true;
								}
							}
						}
					}
			
				}
				else if((invDistX < distX) && (invDistX < invDistY) && (invDistX < invDistY)) { //click was closest to the right side
					if(!board[boxX][boxY].isRight()) { //if this side has not already been clicked, click it
						////System.out.println("Clicked board[" + boxX + "][" + boxY + "] right side.");
						board[boxX][boxY].clickRight();
						if(isServer) {
							//System.out.println("Server clicked right.");
							server.output.println(boxX);
							server.output.flush();
							server.output.println(boxY);
							server.output.flush();
							server.output.println("Right");
							server.output.flush();
						}
						else {
							//System.out.println("Client clicked right.");
							client.output.println(boxX);
							client.output.flush();
							client.output.println(boxY);
							client.output.flush();
							client.output.println("Right");
							client.output.flush();
						}
						if(boxX < width) {
							board[boxX+1][boxY].clickLeft();
						}
					
						if(board[boxX][boxY].isClosed()) {
							if(isServer) {
								board[boxX][boxY].setOwner(server.serverName.charAt(0)); //set the owner 
								completed = true;
								//System.out.println("Server completed a box.");
							}
							else {
								//System.out.println("Client completed a box.");
								board[boxX][boxY].setOwner(client.clientName.charAt(0)); //set the owner 
								completed = true;
							}
						}
						if(boxX != width) {
							if(board[boxX+1][boxY].isClosed()) {
								if(isServer) {
									//System.out.println("Server completed a box.");
									board[boxX+1][boxY].setOwner(server.serverName.charAt(0)); //set the owner 
									completed = true;
								}
								else {
									//System.out.println("Client completed a box.");
									board[boxX+1][boxY].setOwner(client.clientName.charAt(0)); //set the owner 
									completed = true;
								}
							}
						}
					}
					
				}
				else if((invDistY < distX) && (invDistY < invDistX) && (invDistY < distY)) { //click was closest to the bottom 
					if(!board[boxX][boxY].isBot()) { //if this side has not already been clicked, click it
						////System.out.println("Clicked board[" + boxX + "][" + boxY + "] bottom side.");
						board[boxX][boxY].clickBot();	
						if(isServer) {
							//System.out.println("Server clicked bot.");
							server.output.println(boxX);
							server.output.flush();
							server.output.println(boxY);
							server.output.flush();
							server.output.println("Bot");
							server.output.flush();
						}
						else {
							//System.out.println("Client clicked bot.");
							client.output.println(boxX);
							client.output.flush();
							client.output.println(boxY);
							client.output.flush();
							client.output.println("Bot");
							client.output.flush();
						}
						
						if(boxY < length) {
							board[boxX][boxY+1].clickTop();
						}
						
						if(board[boxX][boxY].isClosed()) {
							if(isServer) {
								board[boxX][boxY].setOwner(server.serverName.charAt(0)); //set the owner 
								completed = true;
								//System.out.println("Server completed a box.");
							}
							else {
								//System.out.println("Client completed a box.");
								board[boxX][boxY].setOwner(client.clientName.charAt(0)); //set the owner 
								completed = true;
							}
						}
						if(boxY != length) {
							if(board[boxX][boxY+1].isClosed()) {
								if(isServer) {
									//System.out.println("Server completed a box.");
									board[boxX][boxY+1].setOwner(server.serverName.charAt(0)); //set the owner 
									//System.out.println("Server is getting another turn.");
									completed = true;
								}
								else {
									//System.out.println("Client completed a box.");
									board[boxX][boxY+1].setOwner(client.clientName.charAt(0)); //set the owner 
									completed = true;
								}
							}
						}
					}	
					
				}
				else { //click was ambiguous
					//System.out.println("Invalid click.");
					return;
				}			
				//System.out.println("Game board has been repainted.");
				gamePanel.repaint();
				
				if(completed && isServer) {
					System.out.println("Still my turn.");
					server.output.println("true");
					server.output.flush();
					return;
				}
				else if(completed) {
					System.out.println("Still my turn.");
					client.output.println("true");
					client.output.flush();
					return;
				}
				
				if(isServer) {
					server.output.println("false");
					server.output.flush();
					isTurn = false;
					server.waitForInput();
					isTurn = true;
					return;
				}
				else {
					client.output.println("false");
					client.output.flush();
					isTurn = false;
					client.waitForInput();
					isTurn = true;
					return;
				}
			}
		});
	}
}
