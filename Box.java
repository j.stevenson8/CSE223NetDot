
public class Box {
	private boolean right = false;
	private boolean left = false;
	private boolean top = false;
	private boolean bot = false;
	private char owner = ' ';
	
	public Box() {
		return;
	}
	
	public void clickRight() {
		right = true;
		return;
	}

	public void clickLeft() {
		left = true;
		return;
	}

	public void clickTop() {
		top = true;
		return;
	}

	public void clickBot() {
		bot = true;
		return;
	}
	
	public char getOwner() {
		return owner;
	}

	public void setOwner(char name) {
		owner = name;
		//System.out.println("Box is owned by: " + owner);
		return;
	}
	
	public boolean isRight() {
		return right;
	}

	public boolean isLeft() {
		return left;
	}
	
	public boolean isTop() {
		return top;
	}
	
	public boolean isBot() {
		return bot;
	}

	public boolean isClosed() {
		if(top && bot && left && right) {
			return true;
		}
		return false;
	}
}
