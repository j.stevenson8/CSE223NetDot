import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Client {
	Scanner input;
	PrintWriter output;
	String serverName;
	String clientName;
	Box[][] board;
	GamePanel gamePanel;
	
	public Client(String clientName, String hostName, Box[][] board, GamePanel gamePanel) {
		this.clientName = clientName;
		Socket client;
		this.board = board;
		this.gamePanel = gamePanel;
		
		try {
			client = new Socket(hostName, 1234); //connect to the server
			//System.out.println("Client has connected to server.");
		} catch (Exception e) {
			//System.out.println("Failed to connect to server.");
			e.printStackTrace();
			System.exit(0);
			return;
		}
		
		try {
			input = new Scanner(client.getInputStream()); //create input stream to get info from the server
			//System.out.println("Input stream has been created.");
		} catch (Exception e) {
			//System.out.println("Failed to create input stream.");
			e.printStackTrace();
			System.exit(0);
			return;
		}
		
		try {
			output = new PrintWriter(client.getOutputStream()); //create output stream to send info to the server
			//System.out.println("Output stream has been created.");
		} catch (IOException e) {
			//System.out.println("Failed to create output stream");
			e.printStackTrace();
		}
		
		output.println(clientName);
		output.flush();
		
		//System.out.println("Waiting for server name.");
		String temp = input.nextLine();
		serverName = temp;	
		//System.out.println("Server's name is <" + serverName + ">");

		return;
	}
	
	public void waitForInput() {
		//System.out.println("Waiting for server.");
		String isTurn = "true";
		
		while(isTurn.equals("true")) {
			//System.out.println("Server clicked.");
			int X = Integer.parseInt(input.nextLine()); //get the x-coordinate of the box the client clicked
			int Y = Integer.parseInt(input.nextLine()); //get the y-coordinate of the box the client clicked
			String side = input.nextLine();

			System.out.println(X);
			System.out.println(Y);
			System.out.println(side);
			
			handleClick(X, Y, side);
			isTurn = input.nextLine();
			System.out.println("<" + isTurn + ">");
		}
		//System.out.println("It's my turn now.");
		return;
	}
	
	public void handleClick(int X, int Y, String side) {
		if(side.equals("Left")) {
			board[X][Y].clickLeft();
			System.out.println("Box[" + X + "][" + (Y) + "] left");
			
			if(X != 0) {
				board[X-1][Y].clickRight();
			}
			
			if(board[X][Y].isClosed()) {
				board[X][Y].setOwner(serverName.charAt(0));
			}
			
			if((X != 0) && (board[X-1][Y].isClosed())) {
				board[X-1][Y].setOwner(serverName.charAt(0));
			}
		}
		
		if(side.equals("Right")) {
			board[X][Y].clickRight();
			System.out.println("Box[" + X + "][" + (Y) + "] right");
			
			if(X != 8) {
				board[X+1][Y].clickLeft();
			}
			
			if(board[X][Y].isClosed()) {
				board[X][Y].setOwner(serverName.charAt(0));
			}
			
			if((X != 8) && (board[X+1][Y].isClosed())) {
				board[X+1][Y].setOwner(serverName.charAt(0));
			}
		}
		
		if(side.equals("Top")) {
			board[X][Y].clickTop();
			System.out.println("Box[" + X + "][" + (Y) + "] top");
			
			if(Y != 0) {
				board[X][Y-1].clickBot();
			}
			
			if(board[X][Y].isClosed()) {
				board[X][Y].setOwner(serverName.charAt(0));
			}
			
			if((X != 0) && (board[X][Y-1].isClosed())) {
				board[X][Y-1].setOwner(serverName.charAt(0));
			}
		}

		if(side.equals("Bot")) {
			board[X][Y].clickBot();
			System.out.println("Box[" + X + "][" + (Y) + "] bot");
			
			if(X != 8) {
				board[X][Y+1].clickTop();
			}
			
			if(board[X][Y].isClosed()) {
				board[X][Y].setOwner(serverName.charAt(0));
			}
			
			if(board[X][Y+1].isClosed()) {
				board[X][Y+1].setOwner(serverName.charAt(0));
			}
		}
		gamePanel.repaint();
	}
}
