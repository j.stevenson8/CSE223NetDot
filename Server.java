import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Server {
		Scanner input;
		PrintWriter output;
		String clientName;
		String serverName;
		Box[][] board;
		GamePanel gamePanel;
		
	public Server(String serverName, Box[][] board, GamePanel gamePanel) {
		this.serverName = serverName;
		ServerSocket server;
		Socket client;
		this.board = board;
		this.gamePanel = gamePanel;
		
		try {
			server = new ServerSocket(1234); //create the server at socket 1234
			//System.out.println("Server created.");
		} catch (Exception e) {
			//System.out.println("Failed to create the server.");
			e.printStackTrace();
			System.exit(0);
			return;
		}
		
		try {
			client = server.accept();//wait for someone to connect to the server 
			//System.out.println("Connection has been made with client.");
		} catch (Exception e) {
			//System.out.println("Failed connection with client.");
			e.printStackTrace();
			System.exit(0);
			return;
		} 
		
		try {
			input = new Scanner(client.getInputStream()); //create an input stream to get input from the client
			//System.out.println("Input stream has been created.");
		} catch (Exception e) {
			//System.out.println("Failed to create input stream.");
			e.printStackTrace();
			System.exit(0);
			return;
		}
		
		try {
			output = new PrintWriter(client.getOutputStream()); //create output stream to send info to the client
			//System.out.println("Output stream has been created.");
		} catch (Exception e) {
			//System.out.println("Failed to create output stream.");
			e.printStackTrace();
			System.exit(0);
			return;
		}	
	
		//System.out.println("Waiting for client name.");
		String temp = input.nextLine(); //get client's name
		clientName = temp;
		//System.out.println("Client's name is <" + clientName + ">");
		
		//System.out.println("Sending server name.");
		output.println(serverName); //send the client your name
		output.flush();

		return;
	}

	public void waitForInput() {
		//System.out.println("Waiting for client.");
		String isTurn = "true";
		
		while(isTurn.equals("true")) {
			//System.out.println("Client clicked.");
			int X = Integer.parseInt(input.nextLine()); //get the x-coordinate of the box the client clicked
			int Y = Integer.parseInt(input.nextLine()); //get the y-coordinate of the box the client clicked
			String side = input.nextLine();
			System.out.println(X);
			System.out.println(Y);
			System.out.println(side);
			
			handleClick(X, Y, side);
			isTurn = input.nextLine();
			System.out.println("<" + isTurn + ">");
		}
		//System.out.println("It's my turn now.");
		return;
	}
	
	public void handleClick(int X, int Y, String side) {
		if(side.equals("Left")) {
			board[X][Y].clickLeft();
			System.out.println("Box[" + X + "][" + (Y) + "] left");
			System.out.println("X: " + X);
			if(X != 0) {
				board[X-1][Y].clickRight();
			}
			
			if(board[X][Y].isClosed()) {
				board[X][Y].setOwner(clientName.charAt(0));
			}
			
			if((X != 0) && (board[X-1][Y].isClosed())) {
				board[X-1][Y].setOwner(clientName.charAt(0));
			}
		}
		
		if(side.equals("Right")) {
			board[X][Y].clickRight();
			System.out.println("Box[" + X + "][" + (Y) + "] right");
			
			if(X != 8) {
				board[X+1][Y].clickLeft();
			}
			
			if(board[X][Y].isClosed()) {
				board[X][Y].setOwner(clientName.charAt(0));
			}
			
			if((X != 8) && (board[X+1][Y].isClosed())) {
				board[X+1][Y].setOwner(clientName.charAt(0));
			}
		}
		
		if(side.equals("Top")) {
			board[X][Y].clickTop();
			System.out.println("Box[" + X + "][" + (Y) + "] top");
			
			if(Y != 0) {
				board[X][Y-1].clickBot();
			}
			
			if(board[X][Y].isClosed()) {
				board[X][Y].setOwner(clientName.charAt(0));
			}
			
			if((Y != 0) && (board[X][Y-1].isClosed())) {
				board[X][Y-1].setOwner(clientName.charAt(0));
			}
		}

		if(side.equals("Bot")) {
			board[X][Y].clickBot();
			System.out.println("Box[" + X + "][" + Y + "] bot");
			
			if(X != 8) {
				board[X][Y+1].clickTop();
			}
			
			if(board[X][Y].isClosed()) {
				board[X][Y].setOwner(clientName.charAt(0));
			}
			
			if((Y != 8) && (board[X][Y+1].isClosed())) {
				board[X][Y+1].setOwner(clientName.charAt(0));
			}
		}
		gamePanel.repaint();
	}
	
	
}
