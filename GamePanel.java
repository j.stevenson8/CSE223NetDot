import java.awt.Graphics;
import javax.swing.JPanel;

public class GamePanel extends JPanel {
	Box[][] board;
	private int width;
	private int length;
	
	public GamePanel(Box[][] board, int length, int width) {
		this.length = length;
		this.width = width;
		this.board = board;
		
		return;
	}
	
	public void paint(Graphics g) {
		super.paint(g); //clear surface
		
		int size = 10;
		int X = 10;
		int Y = 10;
		
		for(int i = 0; i <= width; i++) {
			Y = 10;
			for(int j = 0; j <= length; j++) {
				g.fillOval(X, Y, size, size);
				Y = Y + 50;
			}
			X = X + 50;
		}
		
		X = 37;
		Y = 42;
		
		for(int i = 0; i < width; i++) {
			Y = 42;
			for(int j = 0; j < length; j++) {
				g.drawString(String.valueOf(board[i][j].getOwner()), X, Y);
				//System.out.println("Owner of box[" + i + "][" + j + "] is " + board[i][j].getOwner());
				Y = Y + 50;
			}
			X = X + 50;
		}
		
		X = 15;
		Y = 15;
		
		for(int i = 0; i < width; i++) {
			Y = 15;
			for(int j = 0; j < length; j++) {
				if(board[i][j].isLeft()) {
					System.out.println("Board["+ i + "][" + j + "] left side is being printed.");
					//System.out.println(X + " " + Y);
					//System.out.println(i + " " + j);
					
					g.drawLine(X, Y, X, Y + 50);
				}
				if(board[i][j].isRight()) {
					System.out.println("Board["+ i + "][" + j + "] right side is being printed.");
					//System.out.println(X + " " + Y);
					//System.out.println(i + " " + j);

					g.drawLine(X + 50, Y, X + 50, Y + 50);
				}
				if(board[i][j].isTop()) {
					System.out.println("Board["+ i + "][" + j + "] top side is being printed.");
					//System.out.println(X + " " + Y);
					//System.out.println(i + " " + j);

					g.drawLine(X, Y, X + 50, Y);
				}
				if(board[i][j].isBot()) {
					System.out.println("Board["+ i + "][" + j + "] bottom side is being printed.");
					//System.out.println(X + " " + Y);
					//System.out.println(i + " " + j);

					g.drawLine(X, Y + 50, X + 50, Y + 50);
				}
				Y = Y + 50;
			}
			X = X + 50;
		}
		for(int i = 0; i < width; i++) {
			for(int j = 0; j < length; j++) {
				if(board[i][j].isClosed()) {
					System.out.println("Box[" + i + "][" + j +"] is closed.");
				}
			}
		}	
				
	}
}
